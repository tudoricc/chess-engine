
import java.lang.invoke.SwitchPoint;
import java.util.*;


public class Engine {
	public  String starea = new String();
	public  int Culoare,Jucator;
	public  Board tabladesah = new Board();
	public ArrayList<Search> vector = new ArrayList<Search>();
	public  int poz_mutarelin = 2;
	public  int poz_mutarecoloana = 1;
	public  int poz_mutarelin2=7;
	public  int poz_mutarecoloana2 = 1;
	public boolean cul;
	public static int x1sah, y1sah, x2sah, y2sah = 0;

	
	public String getCaracter(int n){
		switch(n){
		case 1:return "a";
		case 2:return "b";
		case 3:return "c";
		case 4:return "d";
		case 5:return "e";
		case 6:return "f";
		case 7:return "g";
		case 8:return "h";
		default:return "0";
		}
	}

	//special number generator Generates a position in a vector 
	//between the first and the last element.
	public int specialRandom(int upper){


		int R = (int) ((Math.random() * (upper-0 ))+0);
		return R;

	}
	/*In functie de culoarea engine-ului facem o mutare.
	 * Negru  mutam de la a7 in jos
	 * Alb mutam de la a2 in sus
	 */
	public String getNextMove(boolean isWhite){
		cul = isWhite;
		if(isWhite){
			return whiteNextMove();
		}
		return blackNextMove();
	}

	/*In blackNextMove facem mutarile corespunzatoarea motorului pe culoarea neagra
	 * mutam de la a7 in jos
	 * Ne folosim si de 2 variabile declarate mai sus pentru a face aceste mutari
	 */
	public String blackNextMove(){
		String mutare = "";
		ArrayList<Search> posibilitati1 = tabladesah.getVector(tabladesah, cul);
		vector = posibilitati1 ;
		int random = specialRandom(vector.size());
		//System.out.println(random);
		if (isSahblack(tabladesah)) {
			
			
			mutare += escapeSahblack();
			tabladesah.show();
			return mutare;
		}
		if (!isSahblack(tabladesah)){
			if (vector.get(random).piesa.nume == "Pion" ){
				
				//System.out.println("esti aici unde ai luat un pion");
				int pozitiex = vector.get(random).pos.x;
				int pozitiey = vector.get(random).pos.y;
				//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
				
				if(tabladesah.getPiesa(pozitiex+1, pozitiey).culoare == 0){
					mutare = ""+traductorBlack(vector.get(random).pos);

					Search mut = new Search();
					mut.piesa = vector.get(random).piesa;
					mut.pos = vector.get(random).pos ;
					mut.pos.setX(pozitiex+1);

					vector.set(random, mut);

					mutare += traductorBlack(vector.get(random).pos);

					tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex+1,pozitiey );
					//random = -1;
					return mutare;
				}

				else if(tabladesah.getPiesa(pozitiex+1, pozitiey+1).culoare==1){
					mutare += traductorBlack(vector.get(random).pos);

					Search mut = new Search();

					mut.piesa = vector.get(random).piesa;
					mut.pos = vector.get(random).pos ;
					mut.pos.setX(pozitiex+1);
					mut.pos.setY(pozitiey+1);

					vector.set(random,mut);

					mutare+=traductorBlack(vector.get(random).pos);
					tabladesah.update(tabladesah, pozitiex, pozitiey,pozitiex+1, pozitiey+1);
					//random = -1;
					return mutare;
				}
				else if(tabladesah.getPiesa(pozitiex+1,pozitiey-1).culoare == 1){
					mutare += traductorBlack(vector.get(random).pos);


					Search mut = new Search();
					mut.piesa = vector.get(random).piesa;
					mut.pos = vector.get(random).pos ;
					mut.pos.setX(pozitiex+1);
					mut.pos.setY(pozitiey-1);

					vector.set(random, mut);
					mutare+=traductorBlack(vector.get(random).pos);
					tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex+1,pozitiey-1);
					//random = -1;
					return mutare;
				}
				else {
					return blackNextMove();
				}
			}
			
		
		//NEBUNUL DE BOGDAN
		else if(vector.get(random).piesa.nume == "Nebun" && vector.get(random).piesa.culoare == 2){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 1, -1, -1};
			int[] vj = {1, -1, -1, 1};

			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 1)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return blackNextMove();
			}

			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());

			return mutare;
		}
		//REGINA DE ALINUTA
		else if(vector.get(random).piesa.nume == "Regina" && vector.get(random).piesa.culoare !=0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 1, -1, -1,1, 0, -1, 0};
			int[] vj = {1, -1, -1, 1,0, 1, 0, -1};

			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 1)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return blackNextMove();
			}

			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());

			return mutare;
		}
		//TURA LUI BOGDAN
		else if(vector.get(random).piesa.nume == "Tura" && vector.get(random).piesa.culoare == 2){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 0, -1, 0};
			int[] vj = {0, 1, 0, -1};

			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 1)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return blackNextMove();
			}

			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
			return mutare;

		}
		else if(vector.get(random).piesa.nume == "Cal" && vector.get(random).piesa.culoare != 0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 2, 2, 1, -1, -2, -2, -1};
			int[] vj = {2, 1, -1, -2, -2, -1, 1, 2};

			for(int i = 0; i < 8; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 1)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else if(tabladesah.getPiesa(pozitiex, pozitiey).culoare==1){
				return blackNextMove();
			}
			else{
				return blackNextMove();
			}

			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
			return mutare;

		}
		else{
			return blackNextMove();
		}
		//return mutare;
		}
	
		else{
			return "resign";
		}
	
	}

	/*In whiteNextMove facem mutarile corespunzatoarea motorului pe culoarea neagra
	 * mutam de la a2 in sus
	 * Ne folosim si de 2 variabile declarate mai sus pentru a face aceste mutari
	 */
	public String whiteNextMove(){
		String mutare = "";
		int random = specialRandom(vector.size());
		if (isSahwhite(tabladesah)){

			mutare += escapeSahwhite();
			tabladesah.show();
			return mutare;
		}
		if (!isSahwhite(tabladesah)){
			if (vector.get(random).piesa.nume == "Pion" ){
			
				int pozitiex = vector.get(random).pos.x;
				int pozitiey = vector.get(random).pos.y;
				
				if(tabladesah.getPiesa(pozitiex-1, pozitiey-1).culoare == 2){
					mutare = "" +traductorWhite(vector.get(random).pos);

					Search mut = new Search();
					mut.piesa = vector.get(random).piesa;
					mut.pos = vector.get(random).pos;
					mut.pos.setX(pozitiex-1);
					mut.pos.setY(pozitiey-1);

					vector.set(random, mut);
					mutare += traductorWhite(vector.get(random).pos);
					tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey-1);
					//random = -1;
					return mutare;
				}
				else if (tabladesah.getPiesa(pozitiex, pozitiey).culoare == 2){
					return whiteNextMove();
				}
				else if(tabladesah.getPiesa(pozitiex-1, pozitiey+1).culoare==2){
					mutare = "" +traductorWhite(vector.get(random).pos);

					Search mut = new Search();
					mut.piesa = vector.get(random).piesa;
					mut.pos = vector.get(random).pos;
					mut.pos.setX(pozitiex-1);
					mut.pos.setY(pozitiey+1);
					vector.set(random, mut);
					mutare += traductorWhite(vector.get(random).pos);
					tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey+1);
					//random = -1;
					return mutare;
				}

				else if (tabladesah.getPiesa(pozitiex-1, pozitiey).culoare == 0){


					mutare = "" +traductorWhite(vector.get(random).pos);

					Search mut = new Search();
					mut.piesa = vector.get(random).piesa;
					mut.pos = vector.get(random).pos;
					mut.pos.setX(pozitiex-1);

					vector.set(random, mut);
					mutare += traductorWhite(vector.get(random).pos);
					tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey);
					//random = -1;
					return mutare;
				}
				else{return whiteNextMove();}
			}
			
		
		else if(vector.get(random).piesa.nume == "Nebun" && vector.get(random).piesa.culoare != 0){
			
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			int[] vi = {1, 1, -1, -1};
			int[] vj = {1, -1, -1, 1};

			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 2)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}
			
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			
			else{
				return whiteNextMove();
			}

			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());

			return mutare;
		}	
		else if(vector.get(random).piesa.nume == "Cal" && vector.get(random).piesa.culoare != 0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			int[] vi = {-1, -2, -2, -1, 1 , 1 };
			int[] vj = {-2, -1, 1 , 2 , 2 , -2};
			
			//nu uita ca ai 2 aici
			for(int i = 0; i < 6; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare ==2)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else if(tabladesah.getPiesa(pozitiex, pozitiey).culoare==2){
				return whiteNextMove();
			}
			
			else{
				return whiteNextMove();
			}

			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());

			return mutare;
		}
		else if(vector.get(random).piesa.nume == "Regina" && vector.get(random).piesa.culoare !=0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 0, -1, 0,1, 1, -1, -1};
			int[] vj = {0, 1, 0, -1,1, -1, -1, 1};

			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 2)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return whiteNextMove();
			}

			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
			return mutare;

		}
		else if(vector.get(random).piesa.nume == "Tura" && vector.get(random).piesa.culoare == 1){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 0, -1, 0};
			int[] vj = {0, 1, 0, -1};

			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 2)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return whiteNextMove();
			}

			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
			return mutare;

		}
		else{return whiteNextMove();}
		}
		else {
			return "resign";
		}

	}
	public boolean isSahblack(Board tabla){
		int i = 0;
		int j = 0;
		int x = 0;
		int y = 0;
		for (i = 1; i <= 8; i ++)
			for (j = 1; j <= 8; j ++)
				if (tabla.tab[i][j].nume == "Rege" && tabla.tab[i][j].culoare == 2){
					x = i;
					y = j;
				}
		for (i = 1; i <= 8; i ++)
			for (j = 1; j <= 8; j ++){
				if (tabla.tab[i][j].culoare == 1){
					switch (tabla.tab[i][j].nume){
					case "Pion": if (x == i - 1 && (y == j - 1 || y == j + 1))
						return true;
					break;
					case "Tura": if (x == i){
						for ( int k = Math.min(y, j) + 1 ; k < Math.max(y, j) ;k++)
							if (tabla.getPiesa(i, k).culoare != 0 )
								return false;
								return true;
					}
					if (y == j){
						for ( int k = Math.min(x, i) + 1; k < Math.max(x, i); k ++)
							if (tabla.getPiesa(k, j).culoare != 0 )
								return false;
								return true;
					}
						
						
					break;
					case "Cal": if (x+2 == i && (y == j - 1 || y == j + 1))
						return true;
					if (y == j - 2 && 	(x == i + 1 || x == i - 1))
						return true;
					if (x == i - 2 && (y == j + 1 || y == j - 1))
						return true;
					if (y == j + 2 && (x == i + 1 || x == i - 1))
						return true;
					break;
					case "Nebun": if ( i - j == x - y){
						for (int k = 1; k < Math.abs(i - x); k ++)
							if (tabla.getPiesa(Math.min(x, i) + k, Math.min(y, j) + k).culoare != 0)
								return false;
								return true;
								
					}
					 if (i + j == x + y){
						 for (int k1 = Math.min(i, x) + 1; k1 < Math.max(i, x); k1 ++)
							 for (int k2 = Math.min(j, y) + 1; k2 < Math.max(j, y); k2 ++)
								 if (k1 + k2 == i + j)
									 if (tabla.getPiesa(k1, k2).culoare != 0)
										 return false;
						 					return true;
					 }
					break;
					case "Regina":if (x == i){
						for ( int k = Math.min(y, j) + 1 ; k < Math.max(y, j) ;k++)
							if (tabla.getPiesa(i, k).culoare != 0 )
								return false;
								return true;
					}
					if (y == j){
						for ( int k = Math.min(x, i) + 1; k < Math.max(x, i); k ++)
							if (tabla.getPiesa(k, j).culoare != 0 )
								return false;
								return true;
					}//Conditie de tura
					 if ( i - j == x - y){
							for (int k = 1; k < Math.abs(i - x); k ++)
								if (tabla.getPiesa(Math.min(x, i) + k, Math.min(y, j) + k).culoare != 0)
									return false;
									return true;
									
						}
						 if (i + j == x + y){
							 for (int k1 = Math.min(i, x) + 1; k1 < Math.max(i, x); k1 ++)
								 for (int k2 = Math.min(j, y) + 1; k2 < Math.max(j, y); k2 ++)
									 if (k1 + k2 == i + j)
										 if (tabla.getPiesa(k1, k2).culoare != 0)
											 return false;
							 					return true;
						 }//Conditie de nebun
						break;
						//break;

					}
				}
			}
		return false;
	}
	public boolean isSahwhite(Board tabla){
		int i = 0;
		int j = 0;
		int x = 0;
		int y = 0;
		for (i = 1; i <= 8; i ++)
			for (j = 1; j <= 8; j ++)
				if (tabla.tab[i][j].nume == "Rege" && tabla.tab[i][j].culoare == 1){
					x = i;
					y = j;
				}
		for (i = 1; i <= 8; i ++)
			for (j = 1; j <= 8; j ++){
				if (tabla.tab[i][j].culoare == 2){
					switch (tabla.tab[i][j].nume){
					case "Pion": if (x == i + 1 && (y == j - 1 || y == j + 1))
						return true;
					break;
					case "Tura": if (x == i){
						for ( int k = Math.min(y, j) + 1 ; k < Math.max(y, j) ;k ++)
							if (tabla.getPiesa(i, k).culoare != 0 )
								return false;
								return true;
					}
					if (y == j){
						for ( int k = Math.min(x, i) + 1; k < Math.max(x, i); k ++)
							if (tabla.getPiesa(k, j).culoare != 0 )
								return false;
								return true;
					}
						break;
					case "Cal": if (x == i - 2 && (y == j - 1 || y == j + 1))
						return true;
					if (y == j + 2 && (x == i - 1 || x == i + 1))
						return true;
					if (x == i + 2 && (y == j - 1 || y == j + 1))
						return true;
					if (y == j - 2 && (x == i - 1 || x == i + 1))
						return true;
					break;
					case "Nebun": if ( i - j == x - y){
						for (int k = 1; k < Math.abs(i - x); k ++)
							if (tabla.getPiesa(Math.min(x, i) + k, Math.min(y, j) + k).culoare != 0)
								return false;
								return true;
								
					}
					 if (i + j == x + y){
						 for (int k1 = Math.min(i, x) + 1; k1 < Math.max(i, x); k1 ++)
							 for (int k2 = Math.min(j, y) + 1; k2 < Math.max(j, y); k2 ++)
								 if (k1 + k2 == i + j)
									 if (tabla.getPiesa(k1, k2).culoare != 0)
										 return false;
						 					return true;
					 }
						break;
					case "Regina": 
						if (x == i){
							for ( int k = Math.min(y, j) + 1 ; k < Math.max(y, j) ;k++)
								if (tabla.getPiesa(i, k).culoare != 0 )
									return false;
									return true;
						}
						if (y == j){
							for ( int k = Math.min(x, i) + 1; k < Math.max(x, i); k ++)
								if (tabla.getPiesa(k, j).culoare != 0 )
									return false;
									return true;
						}
						if ( i - j == x - y){
							for (int k = 1; k < Math.abs(i - x); k ++)
								if (tabla.getPiesa(Math.min(x, i) + k, Math.min(y, j) + k).culoare != 0)
									return false;
									return true;
									
						}
						 if (i + j == x + y){
							 for (int k1 = Math.min(i, x) + 1; k1 < Math.max(i, x); k1 ++)
								 for (int k2 = Math.min(j, y) + 1; k2 < Math.max(j, y); k2 ++)
									 if (k1 + k2 == i + j)
										 if (tabla.getPiesa(k1, k2).culoare != 0)
											 return false;
							 					return true;
						 }
						break;
				


					}
				}
			}
		return false;
	}
	public  void setState(String stare){
		starea=stare;
	}
	/*Construcotrul nostru pentru aceasta clasa
	 * facem si initierea tablei de sah
	 */
	public Engine(){
		Board tabladesah = new Board();

		tabladesah.init();
		Culoare=1; //Punem jucatorul pe negru;
		Jucator=2;
		setState("normal");
	}
	public Engine (int culoareamea){
		Culoare = culoareamea;
		Jucator = 2; //punem jucatorul activ pe 0
		setState("normal");
		Board tabladesah = new Board(); 
		tabladesah.init();
	}
	public static int x =7;

	/*metoda move ne ajuta sa updatam matricea din spate in functie de ce primim
	 * de la winboard,daca de exemplu primim b2b3 punem piesa 
	 * care era la b2:coloana 2 linia 2 la b3 : coloana 2 linia 3
	 */
	public void moveRegina(String mutare){
		//atunci avem o pozitie initiala + pozitie finala
				//avem un string de 4 litere
				//daca de exemplu avem g4f8 separam in g si f si 4 si 8
				int primalitera , adoualitera,opt;
				int pozitie_init , pozitie_fin;

				primalitera = (int)mutare.charAt(0) - 96; //aici mi-ar scoate g-ul
				adoualitera = (int)mutare.charAt(2)-96; //si aici f-ul
				//System.out.println(primalitera + "aceasta este prima litera" + adoualitera + "aceasta este a doua litera");
				opt =mutare.charAt(3);

				pozitie_fin= (int) opt - 48;
				opt = mutare.charAt(1);
				pozitie_init = (int) opt - 48;
				
				//System.out.println("al doilea parametru este:"+pozitie_init + "al 4 lea este:" + pozitie_fin);
				//tabladesah.update_white(tabladesah,primalitera,pozitie_init,adoualitera,pozitie_fin); 
				Piesa aux = tabladesah.getPiesa(adoualitera, pozitie_fin);
				aux.setNume("Regina");
				tabladesah.setPiesa(tabladesah.getPiesa(adoualitera, pozitie_fin), aux);
				tabladesah.setPiesa(tabladesah.getPiesa(primalitera, pozitie_init), new Piesa());
				tabladesah.updateRegina(tabladesah,primalitera,pozitie_init,adoualitera,pozitie_fin);
	}
	public  void move(String mutare){

		//atunci avem o pozitie initiala + pozitie finala
		//avem un string de 4 litere
		//daca de exemplu avem g4f8 separam in g si f si 4 si 8
		int primalitera , adoualitera,opt;
		int pozitie_init , pozitie_fin;

		primalitera = (int)mutare.charAt(0) - 96; //aici mi-ar scoate g-ul
		adoualitera = (int)mutare.charAt(2)-96; //si aici f-ul
		//System.out.println(primalitera + "aceasta este prima litera" + adoualitera + "aceasta este a doua litera");
		opt =mutare.charAt(3);

		pozitie_fin= (int) opt - 48;
		opt = mutare.charAt(1);
		pozitie_init = (int) opt - 48;
		//System.out.println("al doilea parametru este:"+pozitie_init + "al 4 lea este:" + pozitie_fin);
		tabladesah.update_white(tabladesah,primalitera,pozitie_init,adoualitera,pozitie_fin); 



	}
	
	public String escapeSahblack(){
		int x = 0;
		int y = 0;
		int i = 0;
		int j = 0;
		int k = 0;
		for (i = 1; i <= 8; i ++)
			for (j = 1; j <= 8; j ++)
				if (tabladesah.tab[i][j].culoare == 2 && tabladesah.tab[i][j].nume == "Rege"){
					x = i;
					y = j;
				}
		int[] vx = {-1, -1, 0, 1, 1, 1, 0, -1};
		int[] vy = {0, 1, 1, 1, 0, -1, -1, -1};
		Pozitie noua = new Pozitie();
		noua.x = x;
		noua.y = y;
		String mutare = "";
		mutare += traductorBlack(noua);
		
		for (i = 0; i <= 7; i ++){
			if (x + vx[i] <= 8 && x + vx[i] >= 1 && y + vy[i] <= 8 && y + vy[i] >= 1){
				if (tabladesah.tab[x + vx[i]][y + vy[i]].culoare != 2){
					Piesa aux1 = new Piesa("", 0);
					Piesa aux2 = new Piesa("", 0);
					aux1 = tabladesah.tab[x][y];
					aux2 = tabladesah.tab[x + vx[i]][y + vy[i]];
					tabladesah.tab[x + vx[i]][y + vy[i]] = tabladesah.tab[x][y];
					tabladesah.tab[x][y] = new Piesa("", 0);
					Pozitie copie = new Pozitie();
					copie.x = x + vx[i];
					copie.y = y + vy[i];
					
					if(!isSahblack(tabladesah)){
						mutare += traductorBlack(copie);
						return mutare;
					}
					tabladesah.tab[x][y] = aux1;
					tabladesah.tab[x + vx[i]][y + vy[i]] = aux2;
					
				}
		}
		}
		return mutare;
	}
	public String escapeSahwhite(){
		int x = 0;
		int y = 0;
		int i = 0;
		int j = 0;
		int k = 0;
		for (i = 1; i <= 8; i ++)
			for (j = 1; j <= 8; j ++)
				if (tabladesah.tab[i][j].culoare == 1 && tabladesah.tab[i][j].nume == "Rege"){
					x = i;
					y = j;
				}
		int[] vx = {-1, -1, 0, 1, 1, 1, 0, -1};
		int[] vy = {0, 1, 1, 1, 0, -1, -1, -1};
		Pozitie noua = new Pozitie();
		noua.x = x;
		noua.y = y;
		String mutare = "";
		mutare += traductorBlack(noua);
		
		for (i = 0; i <= 7; i ++){
			if (x + vx[i] <= 8 && x + vx[i] >= 1 && y + vy[i] <= 8 && y + vy[i] >= 1){
				if (tabladesah.tab[x + vx[i]][y + vy[i]].culoare != 1){
					Piesa aux1 = new Piesa("", 0);
					Piesa aux2 = new Piesa("", 0);
					aux1 = tabladesah.tab[x][y];
					aux2 = tabladesah.tab[x + vx[i]][y + vy[i]];
					tabladesah.tab[x + vx[i]][y + vy[i]] = tabladesah.tab[x][y];
					tabladesah.tab[x][y] = new Piesa("", 0);
					Pozitie copie = new Pozitie();
					copie.x = x + vx[i];
					copie.y = y + vy[i];
					
					if(!isSahwhite(tabladesah)){
						mutare += traductorWhite(copie);
						return mutare;
					}
					tabladesah.tab[x][y] = aux1;
					tabladesah.tab[x + vx[i]][y + vy[i]] = aux2;
					
				}
		}
		}
		return mutare;
	}
	/*
	tabla.tab[9-apatracifra][atreiacifra] = tabla.tab[9-adouacifra][primacifra];
	tabla.tab[9-adouacifra] [primacifra]= new Piesa("",0);*/
	public String traductorBlack(Pozitie pos ){
		String mutare ="";
		mutare = ""+getCaracter(pos.y) + (9-pos.x);

		return mutare;


	}
	public String traductorWhite(Pozitie pos){
		String mutare ="";
		mutare = "" + getCaracter(pos.y)+ ( 9-pos.x);
		return mutare;
	}
}

