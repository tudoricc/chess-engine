
public class Search {
	Pozitie pos;
	Piesa piesa;
	public Search(){
		this.pos = new Pozitie(0, 0);
		this.piesa = new Piesa("", 0);
	}
	public Search(Pozitie position , Piesa piece){
		this.pos = position;
		this.piesa= piece;
	}
	public String toString(){
		String s = "";
		 s+=this.piesa+" "+this.pos +" ";
		
		return s;
	}
	
}
