README 

Pentru prima etapa noi ne-am folosit de 5 clase si un fisier


Piesa:

Acest obiect este modul in care noi am gandit cum vom reprezenta fiecare piesa
 de pe tabla.CLasa contine un constructor si o metoda toString pentru acest 
obiect particular.


Commands:

Contine variabile globale pe care le folosim in main si variabile care sunt 
comenzile primite de la winboard.Am folosit aceasta clasa pentru a face codul 
mai lizibil si pentru a modulariza codul [un pic].


Board:

Contine reprezentarea interna a tablei de joc .Pentru aceasta etapa ne folosim 
de o matrice de obiecte de tip Piesa.Avem in aceasta clase pentru initierea 
tablei de joc, pentru updatarea matricei , un getter si un constructor mai 
special.


Engine:

Aici ne-am gandit cum sa facem motorul de sah in spate.Avem urmatorii 
descriptori pentru clasa aceasta,starea[string] si Board[tabla de sah din 
spate]

Aceasta clasa contine :
	-o metoda getCaracter care ne ajuta sa convertim literele de la a pana la 
	h cu o cifra
	-o metoda getNextMove : care ne ajuta sa mutam in functie de culoarea pe 
	care o are engineul
	-2 metode de mutare pentru fiecare culoare: blackNextMove si whiteNextMove 
	care muta pionul de pe prima coloana pentru culoarea respectiva.La cele 2 
	metode am folosit 4 variabile globale pentru a-i spune pe ce coloana/linie 
	sa mute.
	-Doi constructori
	-O functie move pe care o folosim pentru a ne updata tabla de sah in 
	functie de ce primim de la WBoard.Noi am folosit forma W(ord)D(ecimal)
	W(ord)D(ecimal).


Main:

Am creeat aceasta clasa pentru a vedea tot ce primim de la winboard si de a 
apela metodele clasei engine.Ne folosim de 2 variabile,una in care avem numele 
motorului si un fisier pentru a citi fluxul de date de la winboard.

Aceasta clasa contine o metoda playGame care face cam tot.Tot in aceasta 
metoda am declarat o variabila isWhite , un string nextmove si un string 
command ,un motor de sah(engine) si un fisier pentru a afisa in el tot ce ne 
trimite WBoard-ul inapoi

Citim in variabila command fiecare linie din fisierul responsabil cu fluxul de 
date si am comparat acest string cu fiecare variabila din clasa Commands in 
felul urmator:
	-sirul vid ,dam break;
	-"quit" ,iesim
	-"new",facem un nou motor 
	-"white" , punem engine-ul pe alb 
	-"black" , punem engine-ul pe negru
	-"xboard" ,afisam un mesaj care ne ajuta atunci cand primim comenzi de move
	-"go" ,facem o miscare in functie de culoarea motorului de sah
	-iar comenzile de forma W(ord)D(ecimal)W(ord)D(ecimal)  le luam ,le 
	prelucram,facem update la matricea noastra din spate si afisam miscarea 
	noastra