
public class Piesa {
	String nume;
	int culoare;
	public Piesa(String numele ,int numarul){
		culoare = numarul;
		nume = numele;
	}
	public Piesa(){
		this.nume="";
		this.culoare = 0;
	}
	public String toString(){
		String s ="";
		s+= this.nume + " " + this.culoare+ " ";
		return s;
	}
	public void setNume(String numenou){
		this.nume= numenou;
	}
	public void setCuloare(int numarnou){
		this.culoare= numarnou;
	}
}
