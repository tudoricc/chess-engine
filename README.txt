Nume Grupa:Super Strong Dinosaurs 
Etapa : 2


Arhiva trimisa contine fisierele .java necesare:Main.java,Board.java,
Engine.java, Piesa.java, Pozitie.java, Search.java


Modificarile fata de etapa trecuta sunt vizibile in fisierul Engine.java si in
Main.java .
De asemenea am adaugat si clase noi precum Pozitie.java , Piesa.java,
Search.java.Pozitie ne ajuta sa luam coordonatele unei piese pe board,Search ne
ajuta in main sa memoram piesele pe care le putem muta in joc,iar Piesa
reprezinta o casuta pe Board,continand numele si culoarea piesei respective.Noi
am realizat aceste clase si pentru aceasta etapa dar si pentru etapele viitoare.


Modificari facute: 

-Am modificat in main ca atunci cand facem o promovare de la
pion la regina in cadrul switch-ului pe ramura default sa se updateze boardul
din spate si sa continuam sa mutam[inainte se bloca pentru ca nu avea aceasta
conditie pusa]. 

-Am eliminat din metoda whitenextmove si blackNextMove
continutul lor 

-In metoda blackNextMove  am adaugat fiecare piesa cu fiecare
mutare corespunzatoare pentru culoarea neagra.Pionul se poate muta in fata ,si
pe diagonala in fata daca are piesa de culoare alba pe care poate sa o aiba, Cal
care se muta in forma literei L dupa regulile sahului , Nebunul se muta cate o
patratica pe diagonala daca se poate , Tura se muta cate o patratica pe
orizontala si pe diagonala. La pion am verificat daca se poate muta o patratica
in fata si daca se putea muta realiza aceasta mutare,daca nu putea si nu avea
nici-o piesa pe diagonala atunci luam alta piesa.[luam inseamna luam random]. La
cal am realizat doi vectori de coordonate posibile pe care ar putea ajunge calul
daca ar face o mutare si verificam care din acesgte mutari este permisa si cele
permise le adaugam intr-un vecotr de mtuari permise din care vom lua o piesa la
intamplare dar daca nu putem lua nici-o piesa luam alta piesa din cele posibile.
La Nebun si la Tura am realizat la fel ca la Cal doar ca de aceast adata mutam
cate o patratica conform regulii de mutare. La regina la fel ca la piesele
precente am facut cei 2 vecotri in care am adaugat coordonatele pe care ar putea
ajunge piesa  iar daca era o mutare valida o punem intr-un vector din care luam
mutari.

-In metoda whiteNextMove am facut la fel ca la blackNextMove, am adaugat fiecare
piesa si am facut mutarile posibile dupa aceleasi reguli.

-Fata de etapa trecuta am creeat si 4 functii auxiliare care ne ajuta sa depistam sahul
pentru fiecare culoare si sa iesim din sah     isSahWhite:Localizam Regele de
alb  si vedem daca cuvma are pe diagonala langa el un pion sau un Cal sau o tura
sau o regina pe linie sau coloana cu el[verificam si daca mai sunt piesa in drum
la regina] si daca se gasesc oricare din aceste piese atunci returnam true;
	
	isSahBlack : a fost creeat dupa acelasi model ca si isSahWhite doar ca pe
	culoarea neagra.
	
	escapeSahwhite: Daca ne aflam in sah incercam sa mutam regele astfel incat
	sa il scoatem din sah.de aceea verificam mutarile posibile ale regelui ,
	iar daca una din mutari nu ne pune in alt sah atunci o
	vom face ,daca nu incercam si alta mutare; 

	escapeSahblack :Realizat dupa acelasi model ca si escapeSahwhite doar ca
	acum suntem pe culoarea neagra.

